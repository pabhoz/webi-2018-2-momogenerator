export interface captionsI {
    x: number,
    y: number,
    value: string
}

export class Momo {

    _captions: Array<captionsI>;
    _src: string;
    _id: any;

    constructor(id, src, captions){
        this.id = id;
        this.src = src;
        this.captions = captions;
    }

    get id(){
        return this._id;
    }

    set id(value){
        this._id = value;
    }

    get src(){
        return this._src;
    }

    set src(value){
        this._src = value;
    }

    get captions(){
        return this._captions;
    }

    set captions(value){
        this._captions = value;
    }

}

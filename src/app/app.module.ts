import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MemeGeneratorComponent } from './meme-generator/meme-generator.component';
import { MemeComponent } from './meme/meme.component';
import { LovifyPipe } from './pipes/lovify.pipe';
import { MomosService } from './services/momos.service';

@NgModule({
  declarations: [
    AppComponent,
    MemeGeneratorComponent,
    MemeComponent,
    LovifyPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HttpClientModule
  ],
  providers: [MomosService],
  bootstrap: [AppComponent]
})
export class AppModule { }

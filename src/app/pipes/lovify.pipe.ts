import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'lovify'
})
export class LovifyPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return `😍 ${JSON.stringify(value)} 😍`;
  }

}

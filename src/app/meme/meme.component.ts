import { Component, OnInit, Input } from '@angular/core';
import { captionsI } from '../models/momo';

@Component({
  selector: 'app-meme',
  templateUrl: './meme.component.html',
  styleUrls: ['./meme.component.css']
})
export class MemeComponent implements OnInit {

  @Input() momosrc: string;
  @Input() captions: Array<captionsI>;

  constructor() { }

  ngOnInit() {
  }

  logItAll(e){
    console.log(e);
  }

}

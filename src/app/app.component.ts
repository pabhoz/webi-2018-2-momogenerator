import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'MomoGenerator';
  isHidden = true;
  btnLabel = "Mostar";
  exBtn = "Cambiar Estado";
  estoySolito = false;
  delDiablo = "Por defecto";

  buitreMsgs = [
    {
      nombre: "Daniela", odio: 8
    },
    {
      nombre: "David", odio: 10
    } 
  ];

  buitres = 4;

  showToggle(e){
    this.isHidden = !this.isHidden;
    this.btnLabel = (this.isHidden) ? "Mostrar" : "Ocultar";
  }

  changeRelationshipStatus(e){
    this.estoySolito = !this.estoySolito;
  }

  update(e){
    this.delDiablo = e.target.value;
  }
}

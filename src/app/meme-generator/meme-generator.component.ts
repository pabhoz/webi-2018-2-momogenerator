import { Component, OnInit } from '@angular/core';
import { Momo } from '../models/momo';
import { MomosService } from '../services/momos.service';

@Component({
  selector: 'app-meme-generator',
  templateUrl: './meme-generator.component.html',
  styleUrls: ['./meme-generator.component.css']
})
export class MemeGeneratorComponent implements OnInit {

  selectedMomo: Momo = new Momo(null,null,null);

  constructor(private momoService: MomosService) { }

  ngOnInit() {
    this.momoService.get().subscribe(()=>{
      this.selectedMomo = this.momoService.momos[0];
    });
    
  }

  changeMomo(e){
    console.log(this.momoService.momos[e.target.id - 1]);
    this.selectedMomo = this.momoService.momos[e.target.id - 1];
  }

  updateCaption(e,id){
    this.selectedMomo.captions[id]["value"] = e.target.value;
  }

}

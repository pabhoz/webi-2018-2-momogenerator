import { TestBed } from '@angular/core/testing';

import { MomosService } from './momos.service';

describe('MomosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MomosService = TestBed.get(MomosService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { Momo } from '../models/momo';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MomosService {

  momos: Array<Momo> = [];

  constructor(private http: HttpClient) { }

  get(){
    return this.http.get('http://localhost/Momoserver/server.php?method=selecionar').pipe(map((data: Array<Momo>) => {
      this.momos = data;
    }));
  }
}
